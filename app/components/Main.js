import React from 'react'
import '../main.css'

const Main = React.createClass({
  render () {
    return (
      <div className='main-container'>
            {this.props.children}
      </div>
    )
  }
});

export default Main
