import React, { PropTypes } from 'react'

//total
function Results ({total, severity}) {
	return (
		<div className="total">
			<h3><span className="severity">{total}</span> - The quiz suggests you have 
				<span className="severity"> {severity}</span> depression.
			</h3>
		</div>
	)
}

Results.propTypes = {
	total: PropTypes.number.isRequired,
	severity: PropTypes.string.isRequired,
}

export default Results

