import React, { PropTypes } from 'react'

function Answer({value, qId, onValueChoice, ans}) {
	// qId needed to establish Id of question in order to update correct value in value array
	return (
		<ul data-qid={qId}> 
			{
				ans.map((answer, index) => {
					return (
						<li
							key={index} 
							onClick={onValueChoice} 
							data-value={index}>
								{answer}
						</li>
					)
				})
			}
		</ul>
	)
}

Answer.propTypes = {
	value: PropTypes.string.isRequired,
	qId: PropTypes.number.isRequired,
	onValueChoice: PropTypes.func.isRequired,
	ans: PropTypes.array.isRequired,
}

export default Answer