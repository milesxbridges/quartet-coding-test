import React, { PropTypes } from 'react'
import Answer from './Answer'
import Contact from './Contact'


function Quiz({total, value, finished, qs, ans, onValueChoice, thanks}) {
	//if user has moderate depression, offer therapist information
	console.log(finished)
	return (
		<div className="wrap">
			{ total > 9 && finished.length === 9 
				? <Contact 
						thanks= {thanks} /> 
				: false  
			} 
			<span>
				<h3>Over the last two weeks, how often have you been bothered by any of the following problems?</h3>
			</span>
			{
				qs.map(function(question, index) {
					return (
						<div className="qa-wrap col-md-4" key={index} data-id={index} >
							<div className="q-wrap" >
								<h3>{question} </h3>
							</div>
							<div className="a-wrap">
								<Answer 
									ans={ans}
									onValueChoice={onValueChoice} 
									value={value[index]} 
									qId = {index} />
							</div>
						</div>
					)
				})
			}
		</div>
	)	
}

Quiz.propTypes = {
	total: PropTypes.number.isRequired,
	value: PropTypes.array.isRequired,
	qs: PropTypes.array.isRequired,
	ans: PropTypes.array.isRequired,
	onValueChoice: PropTypes.func.isRequired,
	thanks: PropTypes.func.isRequired,
	finished: PropTypes.array.isRequired
}


export default Quiz

