import React, { PropTypes } from 'react'

const Contact = React.createClass({
	componentDidMount () {
		(function() {
			document.body.scrollTop = document.documentElement.scrollTop = 0;
		})()
	},
	render() {
		return (
			<div className="therapists col-md-5 col-md-offset-3">
				<h2>Contact a Therapist </h2>
				<ul>
					<li onClick={this.props.thanks}>Ms. Therapist </li>
					<li onClick={this.props.thanks}>Mr. Therapist </li>
					<li onClick={this.props.thanks}>Dr. Therapist </li>
				</ul>
			</div>
		)
	}
})

Contact.propTypes = {
	thanks: PropTypes.func.isRequired
}


export default Contact