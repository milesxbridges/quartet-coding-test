import React from 'react'
import { Router, Route, hashHistory, IndexRoute } from 'react-router'
import Main from '../components/Main'
import QuizContainer from '../containers/QuizContainer'

const routes = (
  <Router history={hashHistory}>
    <Route path='/' component={Main}>
		<IndexRoute component={QuizContainer} />
    </Route>
  </Router>
);

export default routes
