import React from 'react'
import { Link } from 'react-router'
import update from 'immutability-helper';
import Quiz from '../components/Quiz'
import Results from '../components/Results'


const QuizContainer = React.createClass({
	//establish constants: questions, answers, therapists
	getDefaultProps () {
	    return {	
	    	qs: [
	    		'Little interest or pleasure in doing things?',
	    		'Feeling down, depressed, or hopeless?',
	    		'Trouble falling or staying asleep, or sleeping too much?',
	    		'Feeling tired or having little energy?',
	    		'Poor appetite or overeating?',
	    		'Feeling bad about yourself - or that you are a failure or have let yourself or your family down?',
	    		'Trouble concentrating on things, such as reading the newspaper or watching television?',
	    		'Moving or speaking so slowly that other people could have noticed? Or the opposite - being so fidgety or restless that you have been moving around a lot more than usual?',
	    		'Thoughts that you would be better off dead, or of hurting yourself in some way?',
	    	],
	    	ans : [
				'Not at all',
				'Several Days',
				'More than half the days',
				'Nearly every day',
			],
			thanks () {
				let main = document.getElementsByClassName('wrap')[0]
				main.innerHTML = '<h2 class="ty">Thank you for taking the quiz! A Therapist will be in contact soon</h2>'
			}
	    }
	},
	getInitialState () {
		return {
			value: ['0','0','0','0','0','0','0','0', '0'],
			finished: [],
			total : 0,
			severity: 'no'
		}
	},
	componentDidMount () {
		//Array summing function - updates total 
		Array.prototype.sum = function(selector) {
		    if (typeof selector !== 'function') {
		        selector = function(item) {
		            return item;
		        }
		    }
		    var sum = 0;
		    for (var i = 0; i < this.length; i++) {
		        sum += parseFloat(selector(this[i]));
		    }
		    return sum;
		}
		// keep total score of quiz
		this.interval = setInterval(() => {
			let t = this.state.value.sum()
			this.setState({
				total: t
			})
			// determine severity to pass to Results Components
			if(this.state.total < 5) {
				this.setState({
					severity : 'no'
				})
			} else if(this.state.total > 5 && this.state.total < 10) {
				this.setState({
					severity : 'mild'
				})
			} else if (this.state.total > 10 && this.state.total < 15) {
				this.setState({
					severity : 'moderate'
				})
			} else if (this.state.total > 15 && this.state.total < 20) {
				this.setState({
					severity : 'moderately severe'
				})
			} else if (this.state.total > 20) {
				this.setState({
					severity : 'severe'
				})
			}
	    }, 30)
	},
	componentWillUnmount () {
		//clear interval if new routes are added
		window.clearInterval(this.interval)
	},
	handleValueChoice (event, index) {
		//update value in values array with users chosen value
		let updatedState = update(this.state, {
			value : {
				[event.target.parentNode.dataset.qid] : {
					$set: event.target.dataset.value
				}
			},
			finished : {
				[event.target.parentNode.dataset.qid] : {
					$set: event.target.dataset.value
				}
			}
		});
		this.setState(updatedState);


		//give background and font color to users chosen value / remove previous selection
		for(let j=0; j < event.target.parentNode.children.length; j++) {
			event.target.parentNode.children[j].removeAttribute('style')
		}
		event.target.style.color = '#fff';
		event.target.style.backgroundColor = '#d0bf6d'
	},
	render () {
		//render Result & Quiz Components
		return (
			<div className='row'>
				<div className="head-wrap">
					<div className="head">
						<h2><a href="/">Patient Health Questionnaire (PHQ-9)</a></h2>
						<Results
							total = {this.state.total} 
							severity = {this.state.severity} />
					</div>
				</div>
				<Quiz 
					total = {this.state.total} 
					value = {this.state.value}
					qs = {this.props.qs} 
					ans = {this.props.ans} 
					onValueChoice={this.handleValueChoice} 
					thanks = {this.props.thanks} 
					finished = {this.state.finished} />
			</div>
		)
	}
})

export default QuizContainer

