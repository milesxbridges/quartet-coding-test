# Miles Bridges - Quartet ReactJS Exercise - PHQ #

### To deploy locally ###

* Fork this repository then clone your fork 
* cd into your newly cloned directory
* run **npm install** to install the dependencies
* run **npm run start** to run the application locally
* visit http://localhost:8080/


### Approach ###

I began by visiting the [phq-9 test](http://patient.info/doctor/patient-health-questionnaire-phq-9) and deciding what Components the application should be divided into. I decided there would be one parent "QuizContainer" component which would manage the state of the application. "QuizContainer" would have two child components "Quiz" and "Results". Quiz having its own children "Contact, Answer.

I used functional stateless components wherever I could for readability and minimizing the amount of code I needed to write and preferred writing the application in ES6 as some of its new features pair very well with React.

After I wrote the functionality I focused on the design, utilizing bootstrap for a responsive layout. Some of my design choices influenced the functionality and thus the code - causing me to rewrite a few components. For example, after the user took the test, if they scored moderate or higher the prompt for Therapists would appear at the top of the quiz. This meant on mobile I needed to send the user to the top of the page. I added a function for this.

I then tried to break the application, find any bugs, and test all use cases. I believe the app is bug free. 

### Concerns ###
My only concern is the logic behind showing the Therapists contact info. I wanted to show the info only if the user scored above moderate and they answered all of the questions. I did this by evaluating the total score and the length of the 'finished' array which gets updated whenever the user answers a question they haven't answered. The only problem is if the user answers the last question first, the length of the finished array will be 9 and 'finished, and thus the Therapist info will show as soon as the score goes above moderate. Not a huge issue, especially on mobile where the user will likely take the test from the top down, but worth noting.